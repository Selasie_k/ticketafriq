<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <style>
        html * {
            font-family: Nunito !important;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="bg-gray-900 text-gray-200" x-data="{ open: false }">
            <div class="flex justify-between items-center p-4 max-w-screen-xl mx-auto">

                <a class="font-bold text-2xl tracking-wider" href="{{ url('/') }}">
                    <span class="text-orange-500">Ticket</span><span class="text-gray-400">afriq</span> 
                </a>
                <div class="sm:hidden">
                    <button class="block focus:outline-none" x-on:click="open = !open">
                        <svg x-show="open" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><path d="M18 6L6 18M6 6l12 12"/></svg>
                        <svg x-show="!open"width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><path d="M3 12h18M3 6h18M3 18h18"/></svg>
                    </button>
                </div>

                {{-- large screen nav links --}}
                <div class="hidden sm:flex space-x-3">
                    @guest
                        <a class="border-b-2 border-gray-900 hover:border-orange-500" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a class="border-b-2 border-gray-900 hover:border-orange-500" href="{{ route('register') }}">Register</a>
                        @endif
                    @else
                        <a class="border-b-2 border-gray-900 hover:border-orange-500" href="#">{{ Auth::user()->name }}</span></a>

                        <a class="" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </div>
            </div>

            {{-- mobile nav links --}}
            <div class="px-4 pb-4 flex flex-col space-y-3 sm:hidden" x-show.transition="open">
                @guest
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @else
                    <a href="#">{{ Auth::user()->name }}</span></a>

                    <a class="" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
            </div>
        </nav>

        {{-- <ta-navbar
            :is-guest="{{ ! Auth::check() }}"
            user="{{ Auth::user() }}"
            csrf="{{ csrf_token() }}"
        ></ta-navbar> --}}

        <main>
            @yield('content')
        </main>
        {{-- @yield('footer') --}}
    </div>
    <footer class="h-64 bg-black">

    </footer>
</body>
</html>
