<x-app-layout>
    <meta name="viewport" content="width=1024">
    <div class="bg-gray-200">
        <div class="max-w-screen-lg mx-auto py-10 min-h-screen">
            <div class="flex">
                <div class="">
                    <ul class="text-gray-700 capitalize space-y-6">
                        <li class="">
                            <a href="{{route('admin-dash')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/dashboard') ? 'text-orange-500' : '' }}">
                                <app-icon name="pie-chart" :size="4"></app-icon> Dashboard
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin-operators')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/operators') ? 'text-orange-500' : '' }}">
                                <app-icon name="truck" :size="4"></app-icon> Operators
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin-schedules')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/schedules') ? 'text-orange-500' : '' }}">
                                <app-icon name="calendar" :size="4"></app-icon> Schedules
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin-tickets')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/tickets') ? 'text-orange-500' : '' }}">
                                <app-icon name="users" :size="4"></app-icon> Tickets
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin-users')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/users') ? 'text-orange-500' : '' }}">
                                <app-icon name="users" :size="4"></app-icon> Users
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin-dash')}}" class="py-2 px-3 block hover:text-orange-500 {{request()->is('admin/dashboard') ? 'text-orange-500' : '' }}">
                                <app-icon name="settings" :size="4"></app-icon> Settings
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="flex-1 mx-12 px-4">
                    {{$slot}}
                </div>
            </div>
        </div>        
    </div>

</x-app-layout>
