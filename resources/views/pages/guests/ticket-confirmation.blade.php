<x-app-layout>
    <div class="bg-gray-300 py-10 min-h-screen">
        <div class="max-w-sm mx-auto px-4 text-center">

            <div class="mb-16 bg-gray-900 p-4 rounded-lg text-gray-300 flex">
                <div class="h-12 w-12 bg-green-200 rounded-full">
                    <div class="h-full w-full flex-center">
                        <app-icon name="check" :size="8" class="text-green-700 mt-1"></app-icon>
                    </div>
                </div>

                <div class="flex-1 text-left ml-2">
                    <p class="font-medium tracking-widest">Ticket purchased!</p> 
                    <p class="text-sm md:text-xs">
                        Thank you for using our service. Please find your ticket details below.
                        We've also sent a copy to {{$ticket->user->phone}}. Have a safe trip.
                    </p> 
                </div>
            </div>

            <div class="bg-white rounded-lg shadow-lg relative">
                {{-- <div class="h-24 w-24 bg-green-200 absolute inset-x-0 mx-auto -mt-12 rounded-full">
                    <div class="h-full w-full flex-center">
                        <app-icon name="check" :size="16" class="text-green-700"></app-icon>
                    </div>
                </div> --}}
                <div class="card-body py-8 text-center">
                    <p class="text-4xl text-orange-500 tracking-widest">{{$ticket->number}}</p>
                    
                    <div class="flex-center my-4">
                        {!! QrCode::size(170)->generate('ticketafriq.com'); !!}
                    </div>
        
                    <div class="tracking-widest leading-5">
                        <p class="text-gray-600 text-sm">{{$ticket->seats}} Seat(s)</p>
                        <p class="text-gray-600 font-medium">{{$ticket->schedule->from}} - {{$ticket->schedule->to}}</p>
                        <p class="text-gray-600 text-xs">{{$ticket->schedule->date->isoFormat(\App\Utils\Constants::DATE_FORMAT_LONG)}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>