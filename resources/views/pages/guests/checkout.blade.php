<x-app-layout>
    <div class="bg-gray-200 py-10">
        <div class="container min-h-screen">
            <div class="sm:flex sm:space-x-4 items-start">
                
                    <div class="flex-1 hidden sm:block">
                        <img src="/images/man-with-bag 2.jpeg" alt="" class="rounded-lg">
                    </div>

                    <div class="flex-1">
                        @auth
                            <div class="card flex-1 mb-4">
                                <div class="p-4 text-gray-700 border-b font-semibold">Your Information</div>
                                <div class="">
                                    <div class="p-4 flex-1">
                                        <p class="text-orange-600 text-sm">Name</p>
                                        <p class="text-gray-700">{{Auth::user()->name}}</p>
                                    </div>
                                    <div class="p-4 flex-1">
                                        <p class="text-orange-600 text-sm">Email</p>
                                        <p class="text-gray-700">{{Auth::user()->email}}</p>
                                    </div>
                                {{-- </div> --}}
                                {{-- <div class="flex p-4 border-b"> --}}
                                    <div class="p-4 flex-1">
                                        <p class="text-orange-600 text-sm">Phone</p>
                                        <p class="text-gray-700">{{Auth::user()->phone}}</p>
                                    </div>
                                    <div class="flex-1"></div>
                                </div>
                            </div>
                        @else
                            <authenticate></authenticate>
                        @endauth
                    <div class="card mb-4">
                        <div class="">
                            <div class="p-4 text-gray-700 border-b font-semibold">Ticket Details</div>
                            <div class="flex border-b p-4">
                                <div class="flex-1">
                                    <p class="text-orange-600 text-sm">Route</p>
                                    <p class="text-gray-700">{{$schedule->from}} - {{$schedule->to}}</p>
                                </div>
                                <div class="flex-1">
                                    <p class="text-orange-600 text-sm">Operator</p>
                                    <p class="text-gray-700">{{$schedule->operator->name}}</p>
                                </div>
                            </div>
                            <div class="p-4 border-b">
                                <p class="text-orange-600 text-sm">Date</p>
                                <p class="text-gray-700">{{$schedule->date->isoFormat(\App\Utils\Constants::DATE_FORMAT_LONG)}}</p>
                            </div>
                            <div class="flex p-4">
                                <div class="flex-1">
                                    <p class="text-orange-600 text-sm">Reporting</p>
                                    <p class="text-gray-700">{{$schedule->arrival->isoFormat(\App\Utils\Constants::TIME_FORMAT)}}</p>
                                </div>
                                <div class="flex-1">
                                    <p class="text-orange-600 text-sm">Departure</p>
                                    <p class="text-gray-700">{{$schedule->departure->isoFormat(\App\Utils\Constants::TIME_FORMAT)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-4">
                        <checkout-buttons :price="{{$schedule->price}}" :user="{{json_encode(Auth::user())}}" schedule="{{$schedule->token}}"></checkout-buttons>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>