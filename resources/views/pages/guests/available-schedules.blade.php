<x-app-layout>
    <div class="bg-gray-200 py-6 min-h-screen" x-data="{ open: false }">
        <div class="container ">
            <div class="rounded-lg bg-white shadow cursor-pointer p-4">
                <div class="flex justify-between" x-on:click="open = !open">
                    <div>
                        <p class="text-orange-600 text-2xl">{{$data['origin']}} - {{$data['destination']}}</p>
                        <p class="text-sm text-gray-700 block">{{$data['date']}}</p>
                    </div>
                    <button>
                        <app-icon x-show="open" name="chevron-down"></app-icon>
                        <app-icon x-show="!open" name="chevron-right"></app-icon>
                    </button> 
                </div>
                <div x-show.transition="open == true" class="mt-8">
                    <form action="{{ route('find-schedules') }}" method="POST">
                        @csrf
                    
                        <div class="sm:flex justify-between items-center sm:space-x-2">
                            <form-input name="origin" class="flex-1 w-full" label="Origin" type="select" value="{{ old('origin') }}" @error('origin') error="{{$message}}" @enderror required>
                                <template #select_options>
                                    @foreach (config('ticketafriq.cities') as $city)
                                        <option value="{{$city}}" selected>{{$city}}</option>
                                    @endforeach
                                </template>
                            </form-input>
                            
                            <form-input name="destination" class="flex-1 w-full" label="Destination" type="select" value="{{ old('destination') }}" @error('destination') error="{{$message}}" @enderror required>
                                <template #select_options>
                                    @foreach (config('ticketafriq.cities') as $city)
                                        <option value="{{$city}}" selected>{{$city}}</option>
                                    @endforeach
                                </template>
                            </form-input>
                            
                            <form-input class="flex-1 w-full z-50" label-class="text-gray-300" label="Date" type="date" name="date" @error('date') error="{{$message}}" @enderror></form-input>
                            
                            <button type="submit" class="bg-orange-500 p-2 rounded mt-5"> 
                                <span class="text-white">Find Tickets</span> 
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container mt-8">  
            <div class="sm:flex sm:space-x-6">
                <div class="flex-1 hidden sm:block">
                    <img src="/images/bus-stop 2.jpeg" alt="" class="rounded-lg">
                </div>

                <div class="flex-1">
                    @forelse ($schedules as $schedule)
                        <div class="card mb-2 cursor-pointer text-gray-700 flex" onclick="window.location='{{route('checkout', ['schedule' => $schedule->token])}}';">
                            <div class="p-3 flex-1 flex">
                                <img src="/images/bus.png" alt="bus" class="h-12 w-12 border p-2 rounded">
                                <div class="flex-1 ml-2 space-y-1">
                                    <div class="flex justify-between items-start">
                                        <p class="text-sm font-medium uppercase tracking-widest">{{ $schedule->operator->name }}</p>
                                        <p class="text-xs px-2 bg-red-100 text-red-700">{{ $schedule->capacity - $schedule->bookings()->count() }} seats left</p>
                                    </div>
                                    <p class="font-bold">{{ $schedule->from }} - {{ $schedule->to }}</p>
                                    <div class="flex justify-between">
                                        <p class="text-sm text-gray-700">{{ $schedule->date->isoFormat(\App\Utils\Constants::DATE_FORMAT_SHORT) }}</p>
                                        <p class="text-sm text-green-700 font-bold">GHC {{ $schedule->price / 100 }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center items-center pr-2">
                                <app-icon name="chevron-right" :size="6"></app-icon>
                            </div>
                        </div>
                    @empty
                        <div class="card py-32 flex flex-col items-center justify-center text-gray-500">
                            <app-icon name="frown" :size="16"></app-icon>
                            <p class="">Sorry, no buses found</p>   
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</x-app-layout>