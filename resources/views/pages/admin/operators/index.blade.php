<x-admin-dash>
    <div class="card">
        <div class="card-header p-4 text-gray-800 border-b">
            Operators
        </div>
        <div class="">
            <table class="w-full text-left">
                <thead>
                    <tr class="text-gray-800 bg-gray-200">
                        <th class="p-4">#</th>
                        <th class="p-4">Name</th>
                        <th class="p-4">Location</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($operators as $operator)
                        <tr class="font-light">
                            <td class="p-4 text-sm border-b">{{ $operator->id }}</td>
                            <td class="p-4 text-sm border-b">{{ $operator->name }}</td>
                            <td class="p-4 text-sm border-b"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-admin-dash>