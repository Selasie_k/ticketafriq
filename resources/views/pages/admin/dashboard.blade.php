<x-admin-dash>
    <div class="flex space-x-2">
        @foreach ($metrics as $key => $val)
            <div class="flex-1">
                <metric-card title="{{$key}}" value="{{$val}}"></metric-card>
            </div>
        @endforeach
    </div>
</x-admin-dash>