<x-admin-dash>
    <div class="card">
        <div class="card-header p-4 text-gray-800 border-b flex-between">
            <p class="text-2xl">Schehules</p> 
            <button class="btn btn-orange">
                + New Schedule
            </button>
        </div>
        <div class="">
            <table class="w-full text-left">
                <thead>
                    <tr class="text-orange-800 bg-orange-200">
                        <th class="p-4">Operator</th>
                        <th class="p-4">Route</th>
                        <th class="p-4">Bookings</th>
                        <th class="p-4">Seats left</th>
                        <th class="p-4">Price</th>
                        <th class="p-4">Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($schedules as $schedule)
                        <tr class="font-light text-gray-800 hover:bg-gray-200 cursor-pointer" onclick="window.location='{{route('schedule-show', ['schedule' => $schedule->id])}}';">
                        <td class="p-4 text-sm border-b">{{ $schedule->operator->name }}</td>
                        <td class="p-4 text-sm border-b">{{ $schedule->from }} - {{ $schedule->to }}</td>
                        <td class="p-4 text-sm border-b">{{ $schedule->bookings()->count() }}</td>
                        <td class="p-4 text-sm border-b">{{ $schedule->capacity }}</td>
                        <td class="p-4 text-sm border-b">GHC {{ $schedule->price / 100 }}</td>
                        <td class="p-4 text-sm border-b">{{ $schedule->date->isoFormat(\App\Utils\Constants::DATE_FORMAT_SHORT)  }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="p-4">
            {{ $schedules->links() }}
        </div>
    </div>
</x-admin-dash>