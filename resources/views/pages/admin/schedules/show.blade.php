<x-admin-dash>
    <div class="flex mb-6">
        <div class="text-2xl"> Schedule Details </div>
        <div class="flex-1 text-right">
            <a href="#" class="px-3 py-1 bg-blue-400 hover:bg-blue-600 rounded">Edit</a>
            <a href="#" class="px-3 py-1 bg-red-400 hover:bg-red-600 rounded">Delete</a>
        </div>
    </div>
    <div class="mb-6">
        <p class="text-gray-700 mb-2">Route</p>
        <div class="card text-gray-800">
            <div class="card-body flex">
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Origin</p>
                    {{$schedule->from}}
                </div>
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Destination</p>
                    {{$schedule->to}}
                </div>
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Capacity</p>
                    {{$schedule->capacity}}
                </div>
            </div>
        </div>        
    </div>
    <div class="mb-6">
        <p class="text-gray-700 mb-2">Schedule</p>
        <div class="card text-gray-800">
            <div class="card-body flex">
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Travel date</p>
                    {{$schedule->date->isoFormat(\App\Utils\Constants::DATE_FORMAT_LONG) }}
                </div>
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Reporting</p>
                    {{$schedule->arrival->isoFormat(\App\Utils\Constants::TIME_FORMAT) }}
                </div>
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Departure</p>
                    {{$schedule->departure->isoFormat(\App\Utils\Constants::TIME_FORMAT) }}
                </div>
            </div>
        </div>        
    </div>

    <div class="mb-6">
        <p class="text-gray-700 mb-2">Tickets</p>
        <div class="card text-gray-800">
            <div class="card-body flex">
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Price</p>
                    GHC {{$schedule->price / 100}}
                </div>
                <div class="flex-1 text">
                    <p class="text-sm text-gray-500">Bookings</p>
                    {{$schedule->bookings()->count()}}
                </div>
            </div>
        </div>        
    </div>

</x-admin-dash>