<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    /**
     * @param string $token
     */
    public static function findByUuid($uuid)
    {
        if (!$uuid) {
            return ;
        }

        return static::where('uuid', $uuid)->first();
    }
}
