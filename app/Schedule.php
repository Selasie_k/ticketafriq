<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $guarded = [];

    protected $casts = [
        'date' => 'date',
        'arrival' => 'datetime',
        'departure' => 'datetime',
    ];

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function bookings()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * @param string $token
     */
    public static function findByToken($token)
    {
        if (!$token) {
            return ;
        }

        return static::where('token', $token)->first();
    }
}
