<?php

namespace App;

use Illuminate\Support\Facades\Http;

class Flutterwave
{
    protected $endpoint = "https://api.flutterwave.com/v3/payments";

    public function charge(Ticket $ticket)
    {
        return Http::withToken(config('flutterwave.secret_key'))
            ->post($this->endpoint, [
                'public_key' => config('flutterwave.public_key'),
                'tx_ref' => $ticket->uuid,
                'amount' => ($ticket->schedule->price * $ticket->seats) / 100,
                'currency' => 'GHS',
                'payment_options' => 'mobilemoneyghana, card, ussd',
                'redirect_url' => route('payment-redirect'),
                'customer' => [
                    'name' => $ticket->user->name,
                    'email' => $ticket->user->email,
                    'phonenumber' => $ticket->user->phone,
                ],
                'customizations' => [
                    'title' => 'Ticketafriq.com',
                    'description' => "Payment for {$ticket->seats} ticket(s) for route {$ticket->schedule->from} - {$ticket->schedule->to} on {$ticket->schedule->date}"
                ]
            ]);
    }
}
