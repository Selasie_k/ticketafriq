<?php

namespace App\Utils;

class Constants
{
    const DATE_FORMAT_SHORT = 'ddd, MMM D';

    const DATE_FORMAT_LONG = 'dddd, MMM D, YYYY';

    const TIME_FORMAT = 'h:mm A';
}
