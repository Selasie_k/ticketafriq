<?php

namespace App\Http\Controllers;

use App\Operator;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $metrics = [
            'operators' => Operator::count(),
            'users' => User::count()
        ];
        return view('pages.admin.dashboard', ['metrics' => $metrics]);
    }
}
