<?php

namespace App\Http\Controllers;

use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function findSchedules(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'origin' => 'required|string',
            'destination' => 'required|string',
            // 'date' => 'required'
        ]);

        
        $date = $request->filled('date') ? ['=', Carbon::parse($request->date)->format('Y-m-d')] : ['>=', now()];
        
        $schedules = Schedule::where('from', $request->origin)
                        ->where('to', $request->destination)
                        ->whereDate('date', $date[0], $date[1])
                        ->get();
        
        $data = $request->all();
        return view('pages.guests.available-schedules', compact('schedules', 'data'));
    }

    public function store(Schedule $schedule)
    {
        return view('pages.guests.checkout', compact('schedule'));
    }
}
