<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\Ticket;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Schedule $schedule)
    {
        return view('pages.guests.checkout', compact('schedule'));
    }

    public function paymentRedirect(Request $request)
    {
        // dd($request->all());
        $ticket = Ticket::findByUuid($request->tx_ref);
        $ticket->update([
            'transaction_id' => $request->transaction_id,
            'payment_status' => $request->status,
            ]);
        return view('pages.guests.ticket-confirmation', compact('ticket'));
    }
}
