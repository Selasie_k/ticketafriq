<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'is_admin' => 'boolean'
    ];

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function purchaseTicket(Schedule $schedule, int $quantity, string $emergency_name = null, string $emergency_phone = null)
    {
        return $this->tickets()->create([
            'uuid' => (string) Str::uuid(),
            'number' => strtoupper(Str::random(5)),
            'schedule_id' => $schedule->id,
            'seats' => $quantity,
            'emergency_name' => $emergency_name,
            'emergency_phone' => $emergency_phone,
            'discount' => 0,
            'amount' => $schedule->price * $quantity
        ]);
    }
}
