<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Operator;
use App\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
        'token' => \Str::random(60),
        'operator_id' => Operator::all()->random()->id,
        'from' => config('ticketafriq.cities')[rand(0, 3)],
        'to' => config('ticketafriq.cities')[rand(0, 3)],
        'capacity' => rand(10, 50),
        'price' => rand(5000, 11000),
        'date' => now()->addWeek(),
        'arrival' => '05:00:00',
        'departure' => '06:00:00',
    ];
});
