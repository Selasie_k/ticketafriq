<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('number');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('schedule_id')->references('id')->on('schedules');
            $table->integer('seats')->default(1);
            $table->string('emergency_name')->nullable();
            $table->string('emergency_phone')->nullable();
            $table->integer('amount');
            $table->integer('discount')->default(0);
            $table->timestamp('cancelled_on')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('transaction_id')->nullable();
            $table->timestamp('payed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
