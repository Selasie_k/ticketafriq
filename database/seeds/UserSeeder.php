<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Kitsi Paul',
            'email' => 'kitsipaul@ymail.com',
            'phone' => '0542873229',
            'email_verified_at' => now(),
            'is_admin' => true,
            'remember_token' => Str::random(10),
            'password' => \Hash::make('1q2w3e4r')
        ]);

        factory(App\User::class, 20)->create();
    }
}
