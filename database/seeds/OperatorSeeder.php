<?php

use App\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operator::create(['name' => 'VIP']);
        Operator::create(['name' => 'OA']);
        Operator::create(['name' => 'GH Express']);
        Operator::create(['name' => 'VVIP']);
        Operator::create(['name' => 'STC']);
    }
}
