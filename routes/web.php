<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/schedules', 'BookingController@findSchedules')->name('find-schedules');

Route::post('/checkout', 'CheckoutController@store')->name('checkout');

Route::get('/checkout/{schedule:token}', 'CheckoutController@index')->name('checkout');

Route::get('/flutterwave/redirect', 'CheckoutController@paymentRedirect')->name('payment-redirect');


Route::middleware('auth')->group(function () {
    Route::middleware('admin')->group(function () {
        Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin-dash');
        Route::get('/admin/operators', 'OperatorController@index')->name('admin-operators');

        Route::get('/admin/schedules', 'ScheduleController@index')->name('admin-schedules');
        Route::get('/admin/schedules/{schedule}', 'ScheduleController@show')->name('schedule-show');

        Route::get('/admin/tickets', 'TicketController@index')->name('admin-tickets');
        Route::get('/admin/users', 'UserController@index')->name('admin-users');
    });
});
